
package code;

/**
 *
 * @author luise
 */
public class ven1_login extends javax.swing.JFrame {

    /**
     * Creates new form ven1_login
     */
    public ven1_login() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_izquierdo = new javax.swing.JPanel();
        jCPanel1 = new com.bolivia.panel.JCPanel();
        jPanel_derecho = new javax.swing.JPanel();
        jLabel_title = new javax.swing.JLabel();
        jLabel_user = new javax.swing.JLabel();
        txt_usuario = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        txt_contraseña = new javax.swing.JPasswordField();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel_btn_entrar = new javax.swing.JPanel();
        btn_entrar = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        btn_registro = new javax.swing.JLabel();
        btn_facebook = new javax.swing.JLabel();
        btn_instagram = new javax.swing.JLabel();
        btn_youtube = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel_izquierdo.setBackground(new java.awt.Color(255, 102, 255));

        jCPanel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_background/img_03.jpg"))); // NOI18N
        jCPanel1.setVisibleLogo(false);

        javax.swing.GroupLayout jCPanel1Layout = new javax.swing.GroupLayout(jCPanel1);
        jCPanel1.setLayout(jCPanel1Layout);
        jCPanel1Layout.setHorizontalGroup(
            jCPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 349, Short.MAX_VALUE)
        );
        jCPanel1Layout.setVerticalGroup(
            jCPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 450, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel_izquierdoLayout = new javax.swing.GroupLayout(jPanel_izquierdo);
        jPanel_izquierdo.setLayout(jPanel_izquierdoLayout);
        jPanel_izquierdoLayout.setHorizontalGroup(
            jPanel_izquierdoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jCPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
        );
        jPanel_izquierdoLayout.setVerticalGroup(
            jPanel_izquierdoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jCPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
        );

        jPanel_derecho.setBackground(new java.awt.Color(0, 0, 0));
        jPanel_derecho.setPreferredSize(new java.awt.Dimension(400, 500));

        jLabel_title.setFont(new java.awt.Font("Century", 1, 20)); // NOI18N
        jLabel_title.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_title.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_title.setText("Iniciar Sesión");

        jLabel_user.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel_user.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_user.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel_user.setText("USUARIO:");

        txt_usuario.setBackground(new java.awt.Color(0, 0, 0));
        txt_usuario.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txt_usuario.setForeground(new java.awt.Color(255, 255, 255));
        txt_usuario.setText("Ingrese usuario");
        txt_usuario.setBorder(null);

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("CONTRASEÑA:");

        txt_contraseña.setBackground(new java.awt.Color(0, 0, 0));
        txt_contraseña.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txt_contraseña.setForeground(new java.awt.Color(255, 255, 255));
        txt_contraseña.setText("Ingrese contraseña");
        txt_contraseña.setBorder(null);

        btn_entrar.setBackground(new java.awt.Color(255, 255, 255));
        btn_entrar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btn_entrar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_entrar.setText("Entrar");
        btn_entrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_entrar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_entrarMouseMoved(evt);
            }
        });
        btn_entrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_entrarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout jPanel_btn_entrarLayout = new javax.swing.GroupLayout(jPanel_btn_entrar);
        jPanel_btn_entrar.setLayout(jPanel_btn_entrarLayout);
        jPanel_btn_entrarLayout.setHorizontalGroup(
            jPanel_btn_entrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_entrar, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
        );
        jPanel_btn_entrarLayout.setVerticalGroup(
            jPanel_btn_entrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_entrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
        );

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Entrar");

        btn_registro.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btn_registro.setForeground(new java.awt.Color(255, 255, 255));
        btn_registro.setText("Registrarse");
        btn_registro.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_registroMouseMoved(evt);
            }
        });
        btn_registro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_registroMouseExited(evt);
            }
        });

        btn_facebook.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_facebook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_facebook_x32.png"))); // NOI18N
        btn_facebook.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_facebookMouseMoved(evt);
            }
        });
        btn_facebook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_facebookMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_facebookMousePressed(evt);
            }
        });

        btn_instagram.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_instagram.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_instagram_x32.png"))); // NOI18N
        btn_instagram.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_instagramMouseMoved(evt);
            }
        });
        btn_instagram.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_instagramMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_instagramMousePressed(evt);
            }
        });

        btn_youtube.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_youtube.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_youtube_x32.png"))); // NOI18N
        btn_youtube.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                btn_youtubeMouseMoved(evt);
            }
        });
        btn_youtube.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_youtubeMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btn_youtubeMousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel_derechoLayout = new javax.swing.GroupLayout(jPanel_derecho);
        jPanel_derecho.setLayout(jPanel_derechoLayout);
        jPanel_derechoLayout.setHorizontalGroup(
            jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel_title, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_derechoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel_user, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_usuario)
                    .addComponent(txt_contraseña)
                    .addComponent(jSeparator2)
                    .addGroup(jPanel_derechoLayout.createSequentialGroup()
                        .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel_btn_entrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel_derechoLayout.createSequentialGroup()
                                .addComponent(btn_facebook, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(77, 77, 77)
                                .addComponent(btn_instagram, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jSeparator3)
                                .addComponent(btn_registro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(btn_youtube, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)))
                .addGap(50, 50, 50))
            .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel_derechoLayout.createSequentialGroup()
                    .addGap(130, 130, 130)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                    .addGap(131, 131, 131)))
        );
        jPanel_derechoLayout.setVerticalGroup(
            jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_derechoLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel_title, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel_user, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_contraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel_btn_entrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_registro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_facebook, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_instagram, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_youtube, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47))
            .addGroup(jPanel_derechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel_derechoLayout.createSequentialGroup()
                    .addGap(211, 211, 211)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addGap(212, 212, 212)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel_izquierdo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel_derecho, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel_izquierdo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel_derecho, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_entrarMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_entrarMouseMoved
    //Code mostrar border
    btn_entrar.setText("Entrar..");
    btn_entrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }//GEN-LAST:event_btn_entrarMouseMoved

    private void btn_entrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_entrarMouseExited
    //Code ocultar border
    btn_entrar.setText("Entrar");
    btn_entrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    
    }//GEN-LAST:event_btn_entrarMouseExited

    private void btn_registroMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_registroMouseMoved
    //Code
    btn_registro.setText("Registrarse..");
    }//GEN-LAST:event_btn_registroMouseMoved

    private void btn_registroMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_registroMouseExited
    //Code
    btn_registro.setText("Registrase");
    }//GEN-LAST:event_btn_registroMouseExited

    private void btn_facebookMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_facebookMouseMoved
    //Code
    btn_facebook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_facebook_x30.png")));
    }//GEN-LAST:event_btn_facebookMouseMoved

    private void btn_facebookMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_facebookMouseExited
    btn_facebook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_facebook_x32.png")));
    }//GEN-LAST:event_btn_facebookMouseExited

    private void btn_instagramMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_instagramMouseMoved
    btn_instagram.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_instagram_x30.png")));    
    }//GEN-LAST:event_btn_instagramMouseMoved

    private void btn_instagramMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_instagramMouseExited
    btn_instagram.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_instagram_x32.png")));   
    }//GEN-LAST:event_btn_instagramMouseExited

    private void btn_youtubeMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_youtubeMouseMoved
    btn_youtube.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_youtube_x30.png")));   
    }//GEN-LAST:event_btn_youtubeMouseMoved

    private void btn_youtubeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_youtubeMouseExited
    btn_youtube.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_youtube_x32.png")));   
    }//GEN-LAST:event_btn_youtubeMouseExited

    private void btn_facebookMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_facebookMousePressed
    //Code Animacion al presionar el boton
    btn_facebook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_facebook_x32.png")));  
    }//GEN-LAST:event_btn_facebookMousePressed

    private void btn_instagramMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_instagramMousePressed
    //Code Animacion al presionar el boton
    btn_instagram.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_instagram_x32.png")));  
    }//GEN-LAST:event_btn_instagramMousePressed

    private void btn_youtubeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_youtubeMousePressed
    //Code Animacion al presionar el boton
    btn_youtube.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_button/ico_youtube_x32.png"))); 
    }//GEN-LAST:event_btn_youtubeMousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ven1_login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btn_entrar;
    private javax.swing.JLabel btn_facebook;
    private javax.swing.JLabel btn_instagram;
    private javax.swing.JLabel btn_registro;
    private javax.swing.JLabel btn_youtube;
    private com.bolivia.panel.JCPanel jCPanel1;
    private com.bolivia.panel.JCPanel jCPanel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel_title;
    private javax.swing.JLabel jLabel_user;
    private javax.swing.JPanel jPanel_btn_entrar;
    private javax.swing.JPanel jPanel_derecho;
    private javax.swing.JPanel jPanel_izquierdo;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JPasswordField txt_contraseña;
    private javax.swing.JTextField txt_usuario;
    // End of variables declaration//GEN-END:variables
}
